# Getting Started
Welcome to the "embedded" or "real time" repository of Qubo code. This is a
collection of C drivers and applications for talking to sensors and controlling
stuff in a timely manner. We didn't want to worry about installing the
preempt_rt patch or anything like that, so we went baremetal with FreeRTOS.
Specifically, we run a pair of Cortex R5's, each with their own FreeRTOS
instance.

To get started, we need to install some tools.

## Vitis - Installation
Navigate to the [Vitis 2019.2](https://www.xilinx.com/support/download/index.html/content/xilinx/en/downloadNav/vitis/archive-vitis.html)
download page and download "Vitis Core Development Kit", then run the
installer. Vitis is basically a fancy version of Eclipse for Xilinx devices.
Once you have it installed on your system, you need to add all of the binaries
to your path so that the scripts in this repository work.

For bash on linux, I added the following to my `.bashrc`:

```
# Vitis and Vivado path                                                         
export PATH=$PATH:$INSTALL_PATH/Xilinx/Vitis/2019.2/bin                                
export PATH=$PATH:$INSTALL_PATH/Xilinx/Vivado/2019.2/bin 
```

For me, I installed to `~/tools`, but you can install it anywhere.

If you're on windows, you should be able to find how to add stuff to your path
with a quick search.

To test that the tools are installed correctly, go to your terminal and check
that each of the following pulls up a tool or a CLI:
```
vitis  # Should pull up a GUI
vivado # Should pull up a GUI
xsct   # Xilinx Software Command-Line Tool, should pull up a CLI
```

## Vitis - Project Setup
Now we need to create all of the projects that constitute the build system for
the FreeRTOS applications.

In your terminal, run the following:
```
xsct scripts/setup.tcl
vitis -workspace qubo_workspace
```
If everything is alright, you should see Vitis with a welcome screen. You can
just `x` out of it, then you should see a platform project, a system project,
and two application projects (one for each R5) under the system project.

To import our code into the projects, we have to use the GUI. `xsct` currently
does not support import with links, which is what we want so that our code
remains separate from the vitis workspace, so we have to do it manually:
    - Right click on the "freertos_rt_0" application project and select "Import Sources".
    - Fill out the "From Directory" box with `$LOCAL_PATH_TO_REPO/src`, the `src` folder in our repository
    - In the import browser, click the checkmark next to `src`
    - Under options, go to "Advanced", and check "Create links in workspace", "Create virtual folders", and "Create link locations relative to PROJECT_LOC"

You should now have our code tracked by each of the projects. The code in each
of the cores will be identical, except for the main function which will choose
which tasks the core will run (this may change in the future).

## Test Driven Development (TDD)
You may have done unit testing before, you may not have. Here's the main idea:
when coding out a module, you add a test for every piece of functionality that
module provides. This includes edge cases where the functionality should break
or behave in a certain manner. You do not write any code that you don't have a
test for. You add tests, write the code to make the test pass, and proceed in
this incremental manner until your module is complete.

For C, there's a great tool called `ceedling` that provides a test harness and
some mocking tools. 

Ruby is a dependency for `ceedling`:

```
sudo apt install ruby
```

Now, we can use the ruby package manager `gem` to install `ceedling`:

```
sudo gem install ceedling
```

All of our tests are in the `tdd/test` directory. In `tdd/project.yml` you'll
notice that ceedling is pointed to the `src` directory at the root of the
repository. It makes sense to isolate `ceedling` to its own folder since it's a
different build system just for unit testing.
