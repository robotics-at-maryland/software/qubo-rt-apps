/**** R@M 2021 ****/
/*
 * Description: Prototypes and defines for Throttle Control
 *
 * Author: Nathan Renegar
 * Email: naterenegar@gmail.com
 */

// TODO:
//  - Implement bit-banged PWM for our thrusters
//  - We can choose the PWM frequency, but pulse widths must be the same 
//      - Previously it was 200 Hz 
//  - Pulse Width Requirements
//      - Min = 1100 us
//      - Max = 1900 us
//      - Zero Throttle = 1500 us
//  - The more accurate this is the better, but I'd like to be able to hit our
//    times for 8 thrusters within +/- 5 us
//      - This is an arbitrary requirement
//      - To get actual, check ESC PWM curves

void vTask_thrusterPWM(void * pvParameters);
