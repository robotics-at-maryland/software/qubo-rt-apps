/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/* Xilinx includes. */
#include "xil_printf.h"
#include "xparameters.h"

/* Module Includes */
#include "apumsg/apumsg.h"

static TaskHandle_t xAPUMSG_task;

int main(void)
{
    xil_printf("FreeRTOS [r0]: Initializing RPU...\r\n" );

    xTaskCreate(vTask_APUMSG,
            (const char *) "APU Message",
            configMINIMAL_STACK_SIZE,
            NULL,
            tskIDLE_PRIORITY,
            &xAPUMSG_task);

    xil_printf("FreeRTOS [r0]: Tasks created. Starting Scheduler...\r\n");

    /* Start the tasks and timer running. */
    vTaskStartScheduler();

    xil_printf("FreeRTOS [r0]: Broke out of scheduler! Something went wrong!\r\n");

    /* If all is well, the scheduler will now be running, and the following line
    will never be reached.  If the following line does execute, then there was
    insufficient FreeRTOS heap memory available for the idle and/or timer tasks
    to be created.  See the memory management section on the FreeRTOS web site
    for more details. */
    for( ;; );
}


