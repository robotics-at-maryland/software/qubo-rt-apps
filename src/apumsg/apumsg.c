/**** Standard Includes ***/


/**** Module Includes ****/
#include "apumsg.h"

/**** Xilinx Driver Includes ****/
#include <xscugic.h>

/**** FreeRTOS Includes ****/
#include <FreeRTOS.h>
#include <task.h>

/**** libmetal includes ****/
#include <metal/atomic.h>
#include <metal/device.h>
#include <metal/io.h>
#include <metal/irq.h>
#include <metal/sys.h>

/**** Private Constants ****/
#define BUS_NAME        "generic"
#define IPI_DEV_NAME    "ff310000.ipi"
#define SHM_DEV_NAME    "3ed80000.shm"

#define IPI_BASE_ADDR   0xFF310000
#define SHM_BASE_ADDR   0x3ED80000

/* IPI registers offset, from the IPI Module section of UG1087 (v1.7) */
#define IPI_TRIG_OFFSET 0x0  /* IPI trigger reg offset */
#define IPI_OBS_OFFSET  0x4  /* IPI observation reg offset */
#define IPI_ISR_OFFSET  0x10 /* IPI interrupt status reg offset */
#define IPI_IMR_OFFSET  0x14 /* IPI interrupt mask reg offset */
#define IPI_IER_OFFSET  0x18 /* IPI interrupt enable reg offset */
#define IPI_IDR_OFFSET  0x1C /* IPI interrupt disable reg offset */
#define IPI_MASK        0x1000000 /* IPI mask for kick from APU.  We use PL0
                                     IPI in this demo. */
#define IPI_IRQ_VECT_ID       65
#define INTC_DEVICE_ID        XPAR_SCUGIC_0_DEVICE_ID


/**
 * Default generic I/O region page shift 
 * In FreeRTOS, we have a flat address space treated as 1 large page.
 */
#define DEFAULT_PAGE_SHIFT (-1UL)
#define DEFAULT_PAGE_MASK  (-1UL)

/**** External Globals ****/
extern XScuGic xInterruptController; /* Someone should instantiate this
                                        somewhere... it allows us to configure
                                        the GIC */
/**** Private Enums ****/
typedef enum metal_devices {
    IPI_DEV,
    SHM_DEV,
    /* Insert above this line */    
    NUM_METAL_DEVS
} dev_enum;

/**** Private Datatypes ****/
struct metal_devtab {
    struct metal_device      init;
    struct metal_device    * pdev;
    struct metal_io_region * pio;
};

const metal_phys_addr_t m_base_addrs[] = {
    [IPI_DEV] = IPI_BASE_ADDR,
    [SHM_DEV] = SHM_BASE_ADDR
};

/**** Private Variables ****/
/* TODO: Port this to a FreeRTOS semaphore */
static atomic_int remote_nkicked; /* is remote kicked, 0 - kicked,
				       1 - not-kicked */
static int32_t lnum_instances = 0;
static struct metal_devtab m_devtab[NUM_METAL_DEVS] = {
    [IPI_DEV] = {
        .init = {
            .name = IPI_DEV_NAME,
            .bus = NULL,
            .num_regions = 1, /* One contiguous region in physical memory */
            .regions = {
                {
                    .virt = (void *)IPI_BASE_ADDR,
                    .physmap = &m_base_addrs[IPI_DEV],
                    .size = 0x1000,
                    .page_shift = DEFAULT_PAGE_SHIFT,
                    .page_mask = DEFAULT_PAGE_MASK,
                    .mem_flags = DEVICE_NONSHARED | PRIV_RW_USER_RW,
                    .ops = {NULL},
                }
            },
            .node = {NULL},
            .irq_num = 1,
            .irq_info = (void *)IPI_IRQ_VECT_ID,
        }
    },
    [SHM_DEV] = {
        .init = {
            .name = SHM_DEV_NAME,
            .bus = NULL,
            .num_regions = 1,
            .regions = {
                {
                    .virt = (void *)SHM_BASE_ADDR,
                    .physmap = &m_base_addrs[SHM_DEV],
                    .size = 0x1000000,
                    .page_shift = DEFAULT_PAGE_SHIFT,
                    .page_mask = DEFAULT_PAGE_MASK,
                    .mem_flags = NORM_SHARED_NCACHE |
                            PRIV_RW_USER_RW,
                    .ops = {NULL},
                }
            },
            .node = {NULL},
            .irq_num = 0,
            .irq_info = NULL,
        }
    }
};

/**** Private Prototypes ****/
static int register_metal_devices(void);
static int open_metal_devices(void);
static void close_metal_devices(void);
static int init_irq();

/**** Private Functions ****/

/**
 * @brief platform_register_metal_device() - Statically Register libmetal
 *        devices.
 *        This function registers the devices in the dev_tab to the libmetal
 *        generic bus.  Libmetal uses bus structure to group the devices.
 *        Before you can access the device with libmetal device operation, you
 *        will need to register the device to a libmetal supported bus.  For
 *        non-Linux system, libmetal only supports "generic" bus, which is used
 *        to manage the memory mapped devices.
 *
 * @return 0 - succeeded, non-zero for failures.
 */
static int register_metal_devices(void)
{
    unsigned int i;
    int ret;
    struct metal_device *dev;

    for (i = 0; i < NUM_METAL_DEVS; i++)
    {
        dev = &m_devtab[i].init;
        xil_printf("registering: %d, name=%s\n", i, dev->name);
        ret = metal_register_generic_device(dev);
        if (ret)
            return ret;
    }
    return 0;
}

/**
 * @brief open_metal_devices() - Open registered libmetal devices.
 *        This function opens all the registered libmetal devices.
 *
 * @return 0 - succeeded, non-zero for failures.
 */
static int open_metal_devices(void)
{
    unsigned int i;
    int ret;

    for (i = 0; i < NUM_METAL_DEVS; i++)
    {
        ret = metal_device_open(BUS_NAME, m_devtab[i].init.name, &m_devtab[i].pdev);
        if (ret) 
        {
            xil_printf("ERROR: Failed to open device %s.\n", m_devtab[i].init.name);
            break;
        }
    }

    return ret;
}

static void close_metal_devices(void)
{
    for (int i = 0; i < NUM_METAL_DEVS; i++)
    {
        if (m_devtab[i].pdev)
        {
            metal_device_close(m_devtab[i].pdev);
            m_devtab[i].pdev = NULL;
        }
    }
}

/* Adapted from the libmetal FreeRTOS example */
/**
 * @brief init_irq() - Initialize GIC and connect IPI interrupt
 *        This function will initialize the GIC and connect the IPI
 *        interrupt.
 *
 * @return 0 - succeeded, non-0 for failures
 */
static int init_irq()
{
    int ret = 0;
    XScuGic_Config *IntcConfig;    /* The configuration parameters of
                     * the interrupt controller */

    Xil_ExceptionDisable();
    /*
     * Initialize the interrupt controller driver
     */
    IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
    if (NULL == IntcConfig) {
        return (int)XST_FAILURE;
    }

    /* We pass the physical address to the config function since we are
     * baremetal and can access physical addresses */
    ret = XScuGic_CfgInitialize(&xInterruptController, IntcConfig,
                       IntcConfig->CpuBaseAddress);
    if (ret != XST_SUCCESS) {
        return (int)XST_FAILURE;
    }

    /*
     * Register the GIC interrupt handler to the hardware interrupt handling
     * logic in the ARM processor. All the GIC driver handler does is some
     * normal IRQ boilerplate (ACK interrupt, clear IRQ bit, etc.), then jump
     * into our GIC driver registered handler.
     */
    Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT,
            (Xil_ExceptionHandler)XScuGic_InterruptHandler,
            &xInterruptController);

    Xil_ExceptionEnable();

    /* Connect IPI Interrupt ID with libmetal ISR, which also just jumps into a
     * registered handler... Hopefully the compiler can see past all this
     * boilerplate */
    XScuGic_Connect(&xInterruptController, IPI_IRQ_VECT_ID,
               (Xil_ExceptionHandler)metal_xlnx_irq_isr,
               (void *)IPI_IRQ_VECT_ID);

    XScuGic_Enable(&xInterruptController, IPI_IRQ_VECT_ID);

    return 0;
}

/* This is a simple handler... We just set a variable that lets the task know
 * there is a message. Perhaps this should be a FreeRTOS sempahore instead of
 * the libmetal atomic, so that the task actually yields */
static int ipi_irq_handler (int vect_id, void *priv)
{
	(void)vect_id;
	struct metal_io_region *ipi_io = (struct metal_io_region *)priv;
	uint32_t ipi_mask = IPI_MASK;
	uint64_t val = 1;

	if (!ipi_io)
		return METAL_IRQ_NOT_HANDLED;
	val = metal_io_read32(ipi_io, IPI_ISR_OFFSET);
	if (val & ipi_mask) {
		metal_io_write32(ipi_io, IPI_ISR_OFFSET, ipi_mask);
		atomic_flag_clear(&remote_nkicked);
		return METAL_IRQ_HANDLED;
	}
	return METAL_IRQ_NOT_HANDLED;
}

/**** Public Functions ****/
void vTask_APUMSG(void * pvParameters)
{
    const struct metal_init_params metal_param = METAL_INIT_DEFAULTS;
	int ipi_irq;

    if (lnum_instances > 0)
    {
        xil_printf("Multiple APU <--> RPU messaging tasks instantiated.\n");
        xil_printf("Destroying excess task(s).\n");
        vTaskDelete(NULL); // Should return
    }

    lnum_instances++;


    /*** Setup libmetal and IRQs ***/
    init_irq(); /* Config GIC and attach IPI interrupt */

    /* First we initialize the libmetal resource (all this does in FreeRTOS is
     * register a generic bus with libmetal) */
    metal_init(&metal_param);
    metal_xlnx_irq_init();
    register_metal_devices();
    open_metal_devices();

    /* Now that the devices are open, we request I/O regions to write/read from
     **/
    m_devtab[SHM_DEV].pio = metal_device_io_region(m_devtab[SHM_DEV].pdev, 0); 
    m_devtab[IPI_DEV].pio = metal_device_io_region(m_devtab[IPI_DEV].pdev, 0); 

	/* Get the IPI IRQ from the opened IPI device */
	ipi_irq = (intptr_t)m_devtab[IPI_DEV].pdev->irq_info;

	/* disable IPI interrupt */
	metal_io_write32(m_devtab[IPI_DEV].pio, IPI_IDR_OFFSET, IPI_MASK);
	/* clear old IPI interrupt */
	metal_io_write32(m_devtab[IPI_DEV].pio, IPI_ISR_OFFSET, IPI_MASK);
	/* Register IPI irq handler */
	metal_irq_register(ipi_irq, ipi_irq_handler, m_devtab[IPI_DEV].pio);
	metal_irq_enable(ipi_irq);

	/* initialize remote_nkicked */
	atomic_init(&remote_nkicked, 1);
	/* Enable IPI interrupt */
	metal_io_write32(m_devtab[IPI_DEV].pio, IPI_IER_OFFSET, IPI_MASK);

    for (;;)
    {
	    while (atomic_flag_test_and_set(&remote_nkicked));
        // binary semaphore for available message in shmem? 
        // This semaphore is set by the IPI handler 
    }

    /* We should call this stuff when we are powering down... not sure if we
     * will reach this point normally during powerdown / reset */ 
	metal_io_write32(m_devtab[IPI_DEV].pio, IPI_IDR_OFFSET, IPI_MASK);
	metal_irq_disable(ipi_irq);
	metal_irq_unregister(ipi_irq);
    close_metal_devices();
    metal_finish();
    vTaskDelete(NULL);
}
