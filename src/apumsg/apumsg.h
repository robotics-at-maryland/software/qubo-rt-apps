/**** R@M 2021 ****/
/*
 * Description: Prototypes and defines for RPU <--> APU message passing 
 *
 * Author: Nathan Renegar
 * Email: naterenegar@gmail.com
 */

// TODO:
//   Here's the comm chains:
//    APU --> RPU
//      1. APU (ROS Node) writes to Shared Memory
//      2. ROS Node kicks RPU with an IPI
//      3. IPI handler is invoked in the RPU
//      4. Data is handed off to the FreeRTOS apumsg task
//      5. apumsg task makes sure the FreeRTOS system responds to the request
//      appropriately
//      6. (optional) ack is sent RPU --> APU
//    RPU --> APU
//      1. RPU (FreeRTOS task) sends data to apumsg task
//      2. apumsg task writes to shared memory
//      3. apumsg task kicks APU with an IPI
//      4. IPI handler is invoked in the APU
//      5. ROS Node is notified of new data in shared memory
//      6. ROS Node sends data to recipient
//      7. APU takes some (or no) action based on value of data
//      8. (optional) ack is sent APU --> RPU
//
//   - Is it possible to map FreeRTOS structures directly into shared memory?
//      - This may reduce the need for a memcpy by one (i.e. just cast shared
//      memory to a FreeRTOS structure, then act on it??)
//      - Would need to guarantee validity / ownership of the data for some
//      period of time, perhaps until an acknowledge is recieved or the data is
//      released

void vTask_APUMSG(void * pvParameters);
