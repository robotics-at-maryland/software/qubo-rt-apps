/**** R@M 2021 ****/
/*
 * Description: Prototypes and defines for AHRS Sensor Interface 
 *
 * Author: Nathan Renegar
 * Email: naterenegar@gmail.com
 */

// TODO:
//   Here's the comm chains:
//    AHRS --> Us 
//      1. AHRS sends data over UART
//      2. UART sends us an interrupt  
//      3. UART Interrupt Handler is invoked, schedules this task 
//      4. Task puts data into local buffer / broadcasts AHRS data to necessary
//      tasks 
//    Us --> AHRS 
//      1. Someone puts an AHRS command in our mailbox / queue 
//      2. We are invoked, eventually --> check the mailbox
//      3. If there is something in the mailbox, parse the AHRS command struct
//      4. If valid command, execute it by constructing a packet and sending it
//         off the the UART
//
//   - If possible, it would be nice to use DMA for the AHRS --> Us path since
//   that will be utilized much more than the other one
//
//   - I don't think we need to use DMA for the Us --> AHRS path since most of
//   the commands are 
//      - One or very few time config commands, mostly run at startup
//      - Short in length
//   Therefore it will probably be fine to block on sending each byte to the
//   UART rather than sending a packet handle to the DMA controller 
//
//

void vTask_AHRS(void * pvParameters);
