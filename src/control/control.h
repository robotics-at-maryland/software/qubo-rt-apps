/**** R@M 2021 ****/
/*
 * Description: Prototypes and defines for motion control of qubo 
 *
 * Author: Nathan Renegar
 * Email: naterenegar@gmail.com
 */

// TODO:
//   - What are we going to control on the R5, and what are we going to control
//   on the APU?
//      - If nothing, this is just a shim between the APU and thruster control
//      task 
//   - This module interfaces to some thruster control task. It doesn't care
//   how the thrusters are controlled, as long as the throttles match what it
//   wants. We can do this by passing the task a function ptr to a throttle set
//   function in pvParameters 
//

void vTask_control(void * pvParameters);
