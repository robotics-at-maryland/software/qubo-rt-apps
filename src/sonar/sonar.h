/**** R@M 2021 ****/
/*
 * Description: Prototypes and defines for sonar control task 
 *
 * Author: Nathan Renegar
 * Email: naterenegar@gmail.com
 */

// TODO:
//  - Eventually we would like to have this control a DSP IP block in the FPGA
//  - The R5's don't have the NEON SIMD extension like the A53, so doing sonar
//  in realtime on them may be a challenge
//      - We could dedicate a R5 whole core to doing it
//      - If it runs in the hundreds of MHz, may be viable

void vTask_sonar(void * pvParameters);
