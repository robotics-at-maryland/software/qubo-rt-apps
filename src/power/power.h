/**** R@M 2021 ****/
/*
 * Description: Prototypes and defines for system power management 
 *
 * Author: Nathan Renegar
 * Email: naterenegar@gmail.com
 */

// TODO:
//   - I think it's okay for this module to know about the specfic sensor that
//   we're using to measure the power system. This is because each power system
//   has its own requirements and sensors. For example, we might have different
//   voltage lines and number of batteries than a futrue robot, or different
//   ADCs or other sensors for power measurement. While a general task that
//   supports configuration could be made, it is not necessary for this project
//   and does not fit into the schedule. Therefore, this task will be aware of
//   the data format that the ADS ADC outputs.
//
//   - We want to
//      - log current draw
//      - estimate battery charge
//      - estimate time remaining on batteries
//      - estimate available current for thrusters (HIGH PRIORITY)

void vTask_power(void * pvParameters);
