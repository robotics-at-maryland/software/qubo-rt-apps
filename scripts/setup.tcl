# This script sets up the projects necessary to compile our code with vitis
setws qubo_workspace
platform create -name atlas-ii-z8-hp -hw scripts/Xilinx-atlas-ii-z8-hp.xsa

domain create -name "freertos_r5_0" -proc psu_cortexr5_0 -os freertos
bsp setlib -name libmetal
bsp regenerate

domain create -name "freertos_r5_1" -proc psu_cortexr5_1 -os freertos
bsp setlib -name libmetal
bsp regenerate

platform generate
platform write

app create -name "freertos_r5_0" -domain "freertos_r5_0" -sysproj "qubo_r5" -platform atlas-ii-z8-hp -template "FreeRTOS Hello World"
app create -name "freertos_r5_1" -domain "freertos_r5_1" -sysproj "qubo_r5" -platform atlas-ii-z8-hp -template "FreeRTOS Hello World"
